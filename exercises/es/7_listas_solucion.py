#!/usr/bin/python
# coding=utf-8

"""Este program define una lista de nombres y comprueba si el nombre del usario
es en ella.

La comprobación para ver si el nombre del usario se encuentra en la lista no se
ha implementado. Usted debe ponerlo en práctica, a continuación, agregar una
instrucción 'else', que añade el nombre del usario de la lista si no está ya
allí, entonces imprime todos los nombres en la lista.

Usted puede utilizar un bucle en la lista con "for nom in lista:".
"""

lista = ['Angelica', 'Juan', 'Maria', 'Ariel', 'Jorge']
nombre = raw_input('Escribe su nombre: ')

if nombre in lista:
    print('Su nombre es en la lista.')
else:
    lista.append(nombre)
    for nom in lista:
        print(nom)
