#!/usr/bin/python
# coding=utf-8

"""Factorizar un código común a partir de dos métodos en un nuevo método
privado.

Parte del código en la clase a continuación se repite. Busque las dos líneas de
código repetido y moverlos a un nuevo método privado que debe ser llamado desde
los lugares se retiró el código duplicado desde.

Recuerde documentar el nuevo método que utiliza una docstring!
"""

class Counter(object):
    """Un contador que se incrementa o se restablece.

     Esto podría ser utilizado para el recuento de las personas que pasan a
     través de una puerta de entrada, para ejemplo. Cada vez que cambia el
     valor de venta libre, se imprime el mismo mensaje.
    """
    def __init__(self):
        self._counter = 0

    def get_counter(self):
        """Devolver el valor del contador."""
        return self._counter

    def increment_counter(self):
        """Incrementar el valor del contador e imprimir un mensaje."""
        self._counter += 1
        print('Contador es ahora: %i' % self._counter)

    def reset_counter(self):
        """Restablecer el valor del contador a 0 e imprimir un mensaje."""
        self._counter = 0
        print('Contador es ahora: %i' % self._counter)


# Código de probar su clase. No modifique nada por debajo de esta línea.
counter = Counter()
assert(counter.get_counter() == 0)
counter.increment_counter()
assert(counter.get_counter() == 1)
counter.increment_counter()
assert(counter.get_counter() == 2)
counter.reset_counter()
assert(counter.get_counter() == 0)

assert(Counter.__doc__ is not None)
assert(Counter.get_counter.__doc__ is not None)
assert(Counter.increment_counter.__doc__ is not None)
assert(Counter.reset_counter.__doc__ is not None)
