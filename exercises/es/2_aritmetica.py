#!/usr/bin/python
# coding=utf-8

"""A continuación se presentan algunas sumas que demuestran operadores
aritméticos de Python.

Usted debe extender el código para también imprimir del total (suma) y el
promedio (media) de los 5 resultados.

Extensión: Calcular el promedio como un número de coma flotante, en lugar de
como un número entero. Usted encontrará la función float() útil, que es similar
a la función int() utiliza antes.
"""
sumar = 1 + 2 + 3
restar = 10 - 9 - 8
multiplicar = 4 * 5 * 6
dividir = 8 / 4
exponenciar = 2 ** 5

print('Sumas: %i,%i,%i,%i,%i' % (sumar, restar, multiplicar, dividir, exponenciar))
