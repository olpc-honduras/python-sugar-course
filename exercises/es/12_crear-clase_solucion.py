#!/usr/bin/python
# coding=utf-8

"""Defina una clase RegularPolygon que implementa algunos métodos
especificados.

Debe definir una nueva clase, RegularPolygon, para representar un polígono
regular arbitraria. Debe tener los siguientes métodos:
  - __init__(self, lados): que almacena el número de lados del polígono tiene
                           en una variable en la clase
  - get_name(self): que siempre vuelve "Desconocido polígono" (que se utilizará
                    en el ejercicio 13)
  - get_sides(self): que devuelve el número de lados del polígono
  - get_interior_angle(self): que devuelve el tamaño (en grados) de un único
                              ángulo interno en el polígono (por ejemplo, 60
                              para un triángulo, 90 para un cuadrado, etc)
  - get_total_interior_angles(self): que devuelve el ángulo interno total en el
                                     polígono en grados (por ejemplo, 180 para
                                     un triángulo, 360 para un cuadrado, etc)

Recuerde documentar su clase y todos sus métodos utilizando docstrings!
"""

class RegularPolygon(object):
    """Un polígono regular con un número determinado de lados."""
    def __init__(self, sides):
        self._sides = sides

    def get_name(self):
        """Obtener el nombre legible del polígono."""
        return 'Desconocido polígono'

    def get_sides(self):
        """Obtener el número de lados del polígono."""
        return self._sides

    def get_interior_angle(self):
        """Obtener un solo ángulo interior, en grados."""
        return 180 - (360 / self._sides)

    def get_total_interior_angle(self):
        """Obtener la suma de los ángulos interiores en el polígono, en
        grados."""
        return self._sides * self.get_interior_angle()


# Código de probar su clase. No modifique nada por debajo de esta línea.
triangle = RegularPolygon(3)
assert(triangle.get_name() == 'Desconocido polígono')
assert(triangle.get_sides() == 3)
assert(triangle.get_interior_angle() == 60)
assert(triangle.get_total_interior_angle() == 180)

square = RegularPolygon(4)
assert(square.get_name() == 'Desconocido polígono')
assert(square.get_sides() == 4)
assert(square.get_interior_angle() == 90)
assert(square.get_total_interior_angle() == 360)

assert(RegularPolygon.__doc__ is not None)
assert(RegularPolygon.get_name.__doc__ is not None)
assert(RegularPolygon.get_sides.__doc__ is not None)
assert(RegularPolygon.get_interior_angle.__doc__ is not None)
assert(RegularPolygon.get_total_interior_angle.__doc__ is not None)
