#!/usr/bin/python
# coding=utf-8

"""Convertir un par de métodos en una clase como una propiedad en su lugar.

La clase más abajo utiliza un par de métodos, get_size() y set_size(), para
leer y escribir el tamaño del triángulo de Pascal que representa. Debe añadir
una nueva propiedad, tamaño de la clase, que envuelve estos dos métodos.

Usted no tiene que modificar cualquiera de los métodos existentes en la clase.

Recuerde documentar sus adiciones usando docstrings!
"""

class PascalTriangle(object):
    """Una representación del triángulo de Pascal.

    El tamaño (número de células en la base) del triángulo se puede ajustar.
    Los valores de las celdas se almacenan en una lista de tuplas de (índice
    de célula, valor) que se adjunta a cada vez que el usuario rellena
    correctamente una célula.
    """
    def __init__(self, size):
        self._size = size
        self._cell_values = []

    def set_cell_value(self, cell, val):
        """Establecer el valor de la celda dada con la proporcionada por el
        usuario."""
        self._cell_values.append((cell, val))

    def get_size(self):
        """Obtener el tamaño del triángulo."""
        return self._size

    def set_size(self, size):
        """Ajuste el tamaño de triángulo y borrar los valores de las celdas
        introducidos por el usuario."""
        self._size = size
        self._cell_values = []


# Código para revisar sus clases. No modifique nada por debajo de esta línea.
triangle = PascalTriangle(4)
assert(triangle.size == 4)
triangle.size = 5
assert(triangle.size == 5)

assert(PascalTriangle.__doc__ is not None)
assert(PascalTriangle.size.__doc__ is not None)
