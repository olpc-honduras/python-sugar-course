#!/usr/bin/python
# coding=utf-8

import math

"""Definir una función para el cálculo de la media aritmética de una lista de
enteros.

Debe definir una nueva función, arithmetic_mean, que tiene una lista de enteros
como único parámetro, y devuelve un solo número de punto flotante que es la
media aritmética.

Recuerde documentar su función mediante una docstring!

Extensión: Definir una segunda función, geometric_mean, que tiene el mismo
parámetro, pero que devuelve un solo número de punto flotante que es la
media geométrica de los parámetros:
http://es.wikipedia.org/wiki/Media_geométrica. Para ello, tendrá la función
math.pow().
"""

def arithmetic_mean(number_list):
    """Calcular la media aritmética de la lista entero de entrada."""
    total = 0
    count = 0
    for i in number_list:
        total += i
        count += 1
    return float(total) / count

def geometric_mean(number_list):
    """Calcular la media geométrica de la lista entero de entrada."""
    product = 1
    count = 0
    for i in number_list:
        product *= i
        count += 1
    return pow(product, 1.0 / float(count))


# Código de probar sus funciónes. No modifique nada por debajo de esta línea.
assert(arithmetic_mean([5]) == 5)
assert(arithmetic_mean([1, 2, 3]) == 2)
assert(arithmetic_mean([2, 2, 2]) == 2)
assert(arithmetic_mean.__doc__ is not None)

if geometric_mean is not None:
    assert(geometric_mean([5]) == 5)
    assert(geometric_mean([2, 2]) == 2)
    assert(geometric_mean([1, 2, 3]) == pow(6, 1.0 / 3.0))
    assert(geometric_mean.__doc__ is not None)
