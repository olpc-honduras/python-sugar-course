#!/usr/bin/python
# coding=utf-8

import random

"""Este programa debe dividir el número inicial de divisores azar.

Cuando el resultado es menor que 1, el programa debe terminar. El programa es
completar aparte de una instrucción de bucle. Añadir una en la línea indicada
para completar el programa.

Consejo: Un bucle 'for' se repite un número fijo de veces, y por lo tanto puede
ser inadecuado.
"""

num = 100
# Reemplace esta línea con una instrucción de bucle.
    # Imprime el número y lo divide por un número aleatorio entre 2 y 5.
    print(num)
    num = num / random.randint(2, 5)
