#!/usr/bin/python
# coding=utf-8

"""Usted debe escribir código para imprimir los primeros N números en el
secuencia de Fibonacci.

Su programa debe leer N por parte del usario, con el mensaje '¿Cuántos Fibonacci
números deben ser impresas?'. A continuación, deberá imprimir los primeros N
números de la secuencia definida por:
    F_0 = 0
    F_1 = 1
    F_n = F_{n - 1} + F_{n - 2}

Por ejemplo la secuencia: 0, 1, 1, 2, 3, 5, 8, 13, ...
"""
total = int(raw_input('¿Cuántos Fibonacci números deben ser impresas? '))
fn = 0
fm = 1
for i in range(total):
    # Imprimir el número más antiguo que conocemos antes de actualizarlo.
    print(fn)

    # Actualizar los valores fn y fm.
    fn_old = fn
    fn = fm
    fm = fn_old + fm
