#!/usr/bin/python
# coding=utf-8

"""Definir clases Triangle y Square que se extienden RegularPolygon.

Debe definir dos nuevas clases, Triangle y Square, que representan triángulos
regulares y cuadrados. Ambos deben heredar de RegularPolygon.

Además de contar con todos los métodos de RegularPolygon, que más prevalecen
tanto el método de RegularPolygon get_name() para que devuelva 'Triángulo' o
'Cuadrado' en vez de 'Desconocido polígono'.

Recuerde documentar sus clases y sus métodos utilizando docstrings!
"""

# Copie la clase RegularPolygon del ejercicio anterior aquí, sin modificar.

# Añada sus nuevas clases hijas aquí.


# Código de probar sus clases. No modifique nada por debajo de esta línea.
triangle = Triangle()
assert(triangle.get_name() == 'Triángulo')
assert(triangle.get_sides() == 3)
assert(triangle.get_interior_angle() == 60)
assert(triangle.get_total_interior_angle() == 180)

square = Square()
assert(square.get_name() == 'Cuadrado')
assert(square.get_sides() == 4)
assert(square.get_interior_angle() == 90)
assert(square.get_total_interior_angle() == 360)

assert(Triangle.__doc__ is not None)
assert(Triangle.get_name.__doc__ is not None)
assert(Square.__doc__ is not None)
assert(Square.get_name.__doc__ is not None)
