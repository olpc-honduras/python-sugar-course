#!/usr/bin/python
# coding=utf-8

"""Este código le pide al usario su nombre, lo guarda en una variable, luego
imprime de nuevo.

Usted debe extender el código para también pedir al usario su apellido, guárdelo
en un segunda variable, y concatenar al nombre y luego imprimir el nombre
completo del usario.
"""
nombre = raw_input('Escribe su nombre: ')
print('Su nombre es: %s' % nombre)
