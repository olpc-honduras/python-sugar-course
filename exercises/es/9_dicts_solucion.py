#!/usr/bin/python
# coding=utf-8

"""Este programa busca el color favorito del usario o les pregunta al respecto.

Este programa tiene un diccionario que mapea los nombres de los usarios a sus
colores favoritos. Si el usario _no_ es en el diccionario, usted debe agregar
código que pide el usario de su color favorito, y luego agrega una asignación
para el diccionario de el nombre del usario a su color.
"""

colores = {
    'Anna': 'rojo',
    'Jorge': 'verde',
    'Max': 'verde',
    'Felix': 'negro',
    'Ariel': 'amarillo',
}

nombre = raw_input('Escribe su nombre: ')
if nombre in colores:
    print('Su color favorito es: %s' % colores[nombre])
else:
    color = raw_input('Escribe su color favorito: ')
    colores[nombre] = color
