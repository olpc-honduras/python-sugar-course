#!/usr/bin/python
# coding=utf-8

"""Este programa crea una lista de los números, a continuación, debe imprimir
el número mínimo.

Actualmente, sólo se construye una lista de números. Usted debe modificar el
programa para imprimir el número más pequeño de la lista, por ejemplo:
    print('El número más pequeño es: %i' % algo)

Existen varios métodos para hacer esto. El más sencillo es utilizar un método
descrito en la referencia de API para las listas, que encuentra el elemento
mínimo de una lista:
    http://docs.python.org/2.7/library/stdtypes.html#sequence-types-str-unicode-list-tuple-bytearray-buffer-xrange
"""

# Comience con una lista vacía, y añadir 5 números a la misma.
lista = []
for i in range(5):
    lista.append(int(raw_input('Escribe un número: ')))

# Imprimir el elemento mínimo de la lista, utilizando el método min(), que se
# describe aquí:
#     http://docs.python.org/2.7/library/functions.html#min
print('El número más pequeño es: %i' % min(lista))
