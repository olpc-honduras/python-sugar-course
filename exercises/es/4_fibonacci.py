#!/usr/bin/python
# coding=utf-8

"""Usted debe escribir código para imprimir los primeros N números en el
secuencia de Fibonacci.

Su programa debe leer N por parte del usario, con el mensaje '¿Cuántos Fibonacci
números deben ser impresas?'. A continuación, deberá imprimir los primeros N
números de la secuencia definida por:
    F_0 = 0
    F_1 = 1
    F_n = F_{n - 1} + F_{n - 2}

Por ejemplo la secuencia: 0, 1, 1, 2, 3, 5, 8, 13, ...
"""
total = int(raw_input('¿Cuántos Fibonacci números deben ser impresas? '))

# Añadir su código aquí. Consejo: usar dos variables para guardar y actualizar
# los dos valores más recientes de la secuencia.
