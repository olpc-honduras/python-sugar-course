#!/usr/bin/python
# coding=utf-8

"""El c‭ódigo siguiente imprime los 10 primeros números cuadrados.

Usted debe modificar el código para su lugar imprime las 10 primeras potencias
de un número introducido por el usario. Por ejemplo, si el usario introduce '3',
el programa se imprimir los primero 10 poderes de 3 (1, 3, 9, 27, 81, ...).
"""
base = int(raw_input('Escribir un número de la base: '))
for i in range(10):
    print(base ** i)
