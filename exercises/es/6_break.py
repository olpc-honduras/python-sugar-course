#!/usr/bin/python
# coding=utf-8

"""Este programa le pregunta repetidamente al usario un número y lo imprime.

En la actualidad, nunca se dará por terminado: hay que pulsar Ctrl+C para
finalizar el programa. Sin embargo, Python tiene una instrucción 'break' que
hace que la ejecución de abandonar el bucle y continuar la ejecución del código
después del bucle. Por ejemplo, el siguiente código imprimir sólo los números
0, 1, 2, 3, 4. Cuando (i == 5), el condición 'if' es True, y la instrucción
'break' se ejecuta, finalizando el bucle.

    for i in range(10):
        print(i)
        if i >= 5
            break

Modificar el programa a continuación para abandonar el bucle si el usario
escribe un número mayor de 10. Al hacer esto, el programa debe imprimir 'Número
era demasiado grande!'. De lo contrario, si el número es 10 o menos, el programa
debe imprimir 'Su número...' como antes.
"""

while True:
    num = int(raw_input('Escribe un número: '))
    print('Su número: %i' % num)
