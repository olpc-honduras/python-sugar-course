#!/usr/bin/python
# coding=utf-8

"""Definir una función recursiva para calcular el máximo común denominador.

Debe definir una nueva función, mcd (que significa "máximo común denominador"),
que toma dos enteros como parámetros y devuelve un entero que es el máximo común
denominador de los dos. Este es el número entero más grande que divide los dos
números de manera uniforme.

Una manera fácil de implementar esto es usar el algoritmo de Euclides:
     http://es.wikipedia.org/wiki/Algoritmo_de_Euclides

Su función debe ser implementado utilizando recursividad.

Recuerde documentar su función mediante una docstring!
"""

# Escribe su nuevo función aquí.


# Código para probar su función. No modifique nada por debajo de esta línea.
assert(mcd(1, 1) == 1)
assert(mcd(10, 12) == 2)
assert(mcd(12, 10) == 2)
assert(mcd(5, 0) == 5)
assert(mcd(0, 5) == 5)
assert(mcd.__doc__ is not None)
