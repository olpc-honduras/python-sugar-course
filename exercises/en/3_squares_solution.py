#!/usr/bin/python
# coding=utf-8

"""The code below prints the first 10 square numbers.

You must modify the code to instead print the first 10 powers of a number
entered by the user. For example, if the user enters '3', the program will
print the first 10 powers of 3 (1, 3, 9, 27, 81, ...).
"""
base = int(raw_input('Enter a base number: '))
for i in range(10):
    print(base ** i)
