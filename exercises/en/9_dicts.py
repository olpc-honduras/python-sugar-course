#!/usr/bin/python
# coding=utf-8

"""This program looks up the user's favourite colour or asks them about it.

This program has a dictionary which maps users' names to their favourite
colour. If the user is _not_ in the dictionary, you must add code which asks
the user for their favourite colour, then adds a mapping to the dictionary from
the user's name to their colour.
"""

colores = {
    'Anna': 'rojo',
    'Jorge': 'verde',
    'Max': 'verde',
    'Felix': 'negro',
    'Ariel': 'amarillo',
}

nombre = raw_input('Escribir su nombre: ')
if nombre in colores:
    print('Su color favorito es: %s' % colores[nombre])
# Add code here to, if nombre is not in the dictionary, ask the user for their
# favourite colour and add a new entry to the dictionary mapping their name to
# their favourite colour.
