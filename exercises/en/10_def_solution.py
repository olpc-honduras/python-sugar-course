#!/usr/bin/python
# coding=utf-8

import math

"""Define a function to calculate the arithmetic mean of a list of integers.

You must define a new function, arithmetic_mean, which takes a list of integers
as its only parameter, and returns a single floating point number which is
their arithmetic mean.

Remember to document your function using a docstring!

Extension: Define a second function, geometric_mean, which has the same
parameter, but returns a single floating point number which is the geometric
mean of the input integers: http://en.wikipedia.org/wiki/Geometric_mean.
To do this, you’ll need the math.pow() function.
"""

def arithmetic_mean(number_list):
    """Calculate the arithmetic mean of the input integer list."""
    total = 0
    count = 0
    for i in number_list:
        total += i
        count += 1
    return float(total) / count

def geometric_mean(number_list):
    """Calculate the geometric mean of the input integer list."""
    product = 1
    count = 0
    for i in number_list:
        product *= i
        count += 1
    return pow(product, 1.0 / float(count))


# Code to check your functions. Do not modify anything below this line.
assert(arithmetic_mean([5]) == 5)
assert(arithmetic_mean([1, 2, 3]) == 2)
assert(arithmetic_mean([2, 2, 2]) == 2)
assert(arithmetic_mean.__doc__ is not None)

if geometric_mean is not None:
    assert(geometric_mean([5]) == 5)
    assert(geometric_mean([2, 2]) == 2)
    assert(geometric_mean([1, 2, 3]) == pow(6, 1.0 / 3.0))
    assert(geometric_mean.__doc__ is not None)
