#!/usr/bin/python
# coding=utf-8

"""This program repeatedly asks the user for a number and prints it.

Currently, it will never terminate: you must press Ctrl+C to end the program.
However, Python has a 'break' statement which causes execution to leave a loop
and continue with the code below the loop. For example, the following code will
print only the numbers 0, 1, 2, 3, 4. When (i == 5), the 'if' condition is
True, and the break statement is executed, ending the loop.

    for i in range(10):
        print(i)
        if i >= 5
            break

Modify the program below to leave the loop if the user enters a number greater
than 10. When doing so, your program must also print 'Number was too big!'.
Otherwise, if the number is 10 or less, your program must print 'Your
number...' as before.
"""

while True:
    num = int(raw_input('Enter a number: '))
    print('Your number: %i' % num)
