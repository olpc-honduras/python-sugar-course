#!/usr/bin/python
# coding=utf-8

"""This program looks up the user's favourite colour or asks them about it.

This program has a dictionary which maps users' names to their favourite
colour. If the user is _not_ in the dictionary, you must add code which asks
the user for their favourite colour, then adds a mapping to the dictionary from
the user's name to their colour.
"""

colores = {
    'Anna': 'rojo',
    'Jorge': 'verde',
    'Max': 'verde',
    'Felix': 'negro',
    'Ariel': 'amarillo',
}

nombre = raw_input('Escribir su nombre: ')
if nombre in colores:
    print('Su color favorito es: %s' % colores[nombre])
else:
    color = raw_input('Escribir su color favorito: ')
    colores[nombre] = color
