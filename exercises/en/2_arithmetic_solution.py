#!/usr/bin/python
# coding=utf-8

"""Below are a few sums demonstrating Python's arithmetic operators.

You must extend the code to also print the total (sum) and average (mean) of
the 5 results.

Extension: Calculate the average as a floating-point number, rather than as an
integer. You will find the float() function useful, which is similar to the
int() function used before.
"""
add = 1 + 2 + 3
subtract = 10 - 9 - 8
multiply = 4 * 5 * 6
divide = 8 / 4
exponentiate = 2 ** 5

print('Sums: %i,%i,%i,%i,%i' % (add, subtract, multiply, divide, exponentiate))

# Calculate and print the total.
total = add + subtract + multiply + divide + exponentiate
print('Total: %i' % total)

# Calculate and print the integer average.
average = total / 5
print('Average: %i' % average)

# Extension: Calculate and print the floating point average.
floating_average = float(total) / 5.0
print('Floating average: %f' % floating_average)
