#!/usr/bin/python
# coding=utf-8

"""Define Triangle and Square classes which extend RegularPolygon.

You must define two new classes, Triangle and Square, which represent regular
triangles and squares. They must both inherit from RegularPolygon.

In addition to having all the methods from RegularPolygon, they most both
override the get_name() method from RegularPolygon so that it returns 'Triangle'
or 'Square' instead of 'Unknown polygon'.

Remember to document your classes and their methods using docstrings!
"""

# Copy the RegularPolygon class from the previous exercise here, unmodified.

# Add your new child classes here.


# Code to check your classes. Do not modify anything below this line.
triangle = Triangle()
assert(triangle.get_name() == 'Triangle')
assert(triangle.get_sides() == 3)
assert(triangle.get_interior_angle() == 60)
assert(triangle.get_total_interior_angle() == 180)

square = Square()
assert(square.get_name() == 'Square')
assert(square.get_sides() == 4)
assert(square.get_interior_angle() == 90)
assert(square.get_total_interior_angle() == 360)

assert(Triangle.__doc__ is not None)
assert(Triangle.get_name.__doc__ is not None)
assert(Square.__doc__ is not None)
assert(Square.get_name.__doc__ is not None)
