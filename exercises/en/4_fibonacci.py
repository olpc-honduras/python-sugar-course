#!/usr/bin/python
# coding=utf-8

"""You must write code to print the first N numbers in the Fibonacci sequence.

Your program must read N from the user, with the prompt 'How many Fibonacci
numbers should be printed?'. It must then print the first N numbers from the
sequence defined by:
    F_0 = 0
    F_1 = 1
    F_n = F_{n - 1} + F_{n - 2}

e.g. The sequence: 0, 1, 1, 2, 3, 5, 8, 13, ...
"""
total = int(raw_input('How many Fibonacci numbers should be printed? '))

# Add your code here. Hint: use two variables to store most recent two values
# in the sequence.
