#!/usr/bin/python
# coding=utf-8

"""This program builds a list of numbers, then should print the minimum number.

Currently, it only builds a list of numbers. You must modify the program to
print the smallest number in the list, for example:
    print('The smallest number is: %i' % something)

There are several ways to do this. The simplest is to use a method described in
the API reference for lists, which finds the minimum element in a list:
    http://docs.python.org/2.7/library/stdtypes.html#sequence-types-str-unicode-list-tuple-bytearray-buffer-xrange
"""

# Start with an empty list, and append 5 numbers to it.
lista = []
for i in range(5):
    lista.append(int(raw_input('Enter a number: ')))

# Print the minimum element in the list using the min() method, which is
# described here:
#     http://docs.python.org/2.7/library/functions.html#min
print('The smallest number is: %i' % min(lista))
