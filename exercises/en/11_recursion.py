#!/usr/bin/python
# coding=utf-8

"""Define a recursive function to calculate the greatest common denominator.

You must define a new function, gcd (standing for “greatest common
denominator”), which takes two integers as its parameters, and returns a single
integer which is the greatest common denominator of the two. This is the
largest integer which divides both numbers evenly.

An easy way to implement this is using the Euclidean algorithm:
    http://en.wikipedia.org/wiki/Euclidean_algorithm

Your function must be implemented using recursion.

Remember to document your function using a docstring!
"""

# Write your new function here.


# Code to check your function. Do not modify anything below this line.
assert(gcd(1, 1) == 1)
assert(gcd(10, 12) == 2)
assert(gcd(12, 10) == 2)
assert(gcd(5, 0) == 5)
assert(gcd(0, 5) == 5)
assert(gcd.__doc__ is not None)
