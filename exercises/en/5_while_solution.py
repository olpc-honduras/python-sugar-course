#!/usr/bin/python
# coding=utf-8

import random

"""This program should divide the initial number by random divisors.

Once the result is smaller than 1, the program should stop. The program is
complete apart from a loop statement. Add one on the indicated line to complete
the program.

Hint: A 'for' loop iterates a fixed number of times, and so might be
unsuitable.
"""

num = 100
while num >= 1:
    # This prints the number, then divides it by a random number
    # between 2 and 5.
    print(num)
    num = num / random.randint(2, 5)
