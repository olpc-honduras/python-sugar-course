#!/usr/bin/python
# coding=utf-8

"""Define Triangle and Square classes which extend RegularPolygon.

You must define two new classes, Triangle and Square, which represent regular
triangles and squares. They must both inherit from RegularPolygon.

In addition to having all the methods from RegularPolygon, they most both
override the get_name() method from RegularPolygon so that it returns 'Triangle'
or 'Square' instead of 'Unknown polygon'.

Remember to document your classes and their methods using docstrings!
"""

class RegularPolygon(object):
    """A regular polygon with a specified number of sides."""
    def __init__(self, sides):
        self._sides = sides

    def get_name(self):
        """Get the human-readable name of the polyon."""
        return 'Unknown polygon'

    def get_sides(self):
        """Get the number of sides in the polygon."""
        return self._sides

    def get_interior_angle(self):
        """Get a single interior angle, in degrees."""
        return 180 - (360 / self._sides)

    def get_total_interior_angle(self):
        """Get the sum of the interior angles in the polygon, in degrees."""
        return self._sides * self.get_interior_angle()

class Triangle(RegularPolygon):
    """A regular, equilateral triangle."""
    def __init__(self):
        super(Triangle, self).__init__(3)

    def get_name(self):
        """Get the human-readable name of the triangle."""
        return 'Triangle'

class Square(RegularPolygon):
    """A regular square."""
    def __init__(self):
        super(Square, self).__init__(4)

    def get_name(self):
        """Get the human-readable name of the square."""
        return 'Square'


# Code to check your classes. Do not modify anything below this line.
triangle = Triangle()
assert(triangle.get_name() == 'Triangle')
assert(triangle.get_sides() == 3)
assert(triangle.get_interior_angle() == 60)
assert(triangle.get_total_interior_angle() == 180)

square = Square()
assert(square.get_name() == 'Square')
assert(square.get_sides() == 4)
assert(square.get_interior_angle() == 90)
assert(square.get_total_interior_angle() == 360)

assert(Triangle.__doc__ is not None)
assert(Triangle.get_name.__doc__ is not None)
assert(Square.__doc__ is not None)
assert(Square.get_name.__doc__ is not None)
