#!/usr/bin/python
# coding=utf-8

"""You must write code to print the first N numbers in the Fibonacci sequence.

Your program must read N from the user, with the prompt 'How many Fibonacci
numbers should be printed?'. It must then print the first N numbers from the
sequence defined by:
    F_0 = 0
    F_1 = 1
    F_n = F_{n - 1} + F_{n - 2}

e.g. The sequence: 0, 1, 1, 2, 3, 5, 8, 13, ...
"""
total = int(raw_input('How many Fibonacci numbers should be printed? '))
fn = 0
fm = 1
for i in range(total):
    # Print the oldest number we know before updating it.
    print(fn)

    # Update the fn and fm values.
    fn_old = fn
    fn = fm
    fm = fn_old + fm
