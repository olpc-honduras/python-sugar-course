#!/usr/bin/python
# coding=utf-8

"""This code asks the user for their name, stores it in a variable, then prints
it again.

You must extend the code to also ask the user for their surname, store it in a
second variable, and concatenate it to the forename to print out the user's
full name.
"""
forename = raw_input('Enter your forename: ')
surname = raw_input('Enter your surname: ')
print('Your name is: %s' % (forename + ' ' + surname))
