#!/usr/bin/python
# coding=utf-8

"""Factor out some common code from two methods into a new private method.

Some of the code in the class below is repeated. Find the two lines of repeated
code and move them into a new private method which should be called from the
locations the duplicated code was removed from.

Remember to document your new method using a docstring!
"""

class Counter(object):
    """A counter which can be incremented or reset.

    This could be used for counting people passing through an entry gate, for
    example. Whenever the counter value changes, the same message is printed.
    """
    def __init__(self):
        self._counter = 0

    def get_counter(self):
        """Return the counter value."""
        return self._counter

    def increment_counter(self):
        """Increment the counter value and print a message."""
        self._counter += 1
        print('Counter is now: %i' % self._counter)

    def reset_counter(self):
        """Reset the counter value to 0 and print a message."""
        self._counter = 0
        print('Counter is now: %i' % self._counter)


# Code to check your class. Do not modify anything below this line.
counter = Counter()
assert(counter.get_counter() == 0)
counter.increment_counter()
assert(counter.get_counter() == 1)
counter.increment_counter()
assert(counter.get_counter() == 2)
counter.reset_counter()
assert(counter.get_counter() == 0)

assert(Counter.__doc__ is not None)
assert(Counter.get_counter.__doc__ is not None)
assert(Counter.increment_counter.__doc__ is not None)
assert(Counter.reset_counter.__doc__ is not None)
