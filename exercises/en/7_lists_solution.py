#!/usr/bin/python
# coding=utf-8

"""This program defines a list of names and checks whether the user's name is
in it.

The check to see if the user's name is in the list is not implemented. You must
implement it, then add an 'else' statement which appends the user's name to the
list if it's not already there then prints all the names on the list.

You can loop over the list using "for nom in lista:".
"""

lista = ['Angelica', 'Juan', 'Maria', 'Ariel', 'Jorge']
nombre = raw_input('Escribir su nombre: ')

if nombre in lista:
    print('Su nombre es en la lista.')
else:
    lista.append(nombre)
    for nom in lista:
        print(nom)
