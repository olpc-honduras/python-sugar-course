#!/usr/bin/python
# coding=utf-8

"""Convert a pair of methods in a class to be a property instead.

The class below uses a pair of methods, get_size() and set_size(), to read and
write the size of the Pascal triangle it represents. You must add a new
property, size, to the class, which wraps these two methods.

You do not need to modify any of the existing methods in the class.

Remember to document your additions using docstrings!
"""

class PascalTriangle(object):
    """A representation of Pascal's triangle.

    The size (number of cells on the base) of the triangle can be set. The
    values of the cells are stored in a list of tuples of (cell index, value)
    which is appended to whenever the user correctly fills out a cell.
    """
    def __init__(self, size):
        self._size = size
        self._cell_values = []

    def set_cell_value(self, cell, val):
        """Set the value of the given cell to one provided by the user."""
        self._cell_values.append((cell, val))

    def get_size(self):
        """Get the triangle size."""
        return self._size

    def set_size(self, size):
        """Set the triangle size and clear any user-entered cell values."""
        self._size = size
        self._cell_values = []


# Code to check your classes. Do not modify anything below this line.
triangle = PascalTriangle(4)
assert(triangle.size == 4)
triangle.size = 5
assert(triangle.size == 5)

assert(PascalTriangle.__doc__ is not None)
assert(PascalTriangle.size.__doc__ is not None)
