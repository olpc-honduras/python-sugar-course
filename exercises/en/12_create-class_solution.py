#!/usr/bin/python
# coding=utf-8

"""Define a RegularPolygon class which implements some specified methods.

You must define a new class, RegularPolygon, to represent an arbitrary regular
polygon. It must have the following methods:
 - __init__(self, sides): which stores the number of sides the polygon has
                          in a variable in the class
 - get_name(self): which always returns 'Unknown polygon' (it will be used in
                   exercise 13)
 - get_sides(self): which returns the number of sides in the polygon
 - get_interior_angle(self): which returns the size (in degrees) of a single
                             internal angle in the polygon (e.g. 60 for a
                             triangle, 90 for a square, etc.)
 - get_total_interior_angles(self): which returns the total internal angle in
                                    the polygon in degrees (e.g. 180 for a
                                    triangle, 360 for a square, etc.)

Remember to document your class and all its methods using docstrings!
"""

class RegularPolygon(object):
    """A regular polygon with a specified number of sides."""
    def __init__(self, sides):
        self._sides = sides

    def get_name(self):
        """Get the human-readable name of the polygon."""
        return 'Unknown polygon'

    def get_sides(self):
        """Get the number of sides in the polygon."""
        return self._sides

    def get_interior_angle(self):
        """Get a single interior angle, in degrees."""
        return 180 - (360 / self._sides)

    def get_total_interior_angle(self):
        """Get the sum of the interior angles in the polygon, in degrees."""
        return self._sides * self.get_interior_angle()


# Code to check your class. Do not modify anything below this line.
triangle = RegularPolygon(3)
assert(triangle.get_name() == 'Unknown polygon')
assert(triangle.get_sides() == 3)
assert(triangle.get_interior_angle() == 60)
assert(triangle.get_total_interior_angle() == 180)

square = RegularPolygon(4)
assert(square.get_name() == 'Unknown polygon')
assert(square.get_sides() == 4)
assert(square.get_interior_angle() == 90)
assert(square.get_total_interior_angle() == 360)

assert(RegularPolygon.__doc__ is not None)
assert(RegularPolygon.get_name.__doc__ is not None)
assert(RegularPolygon.get_sides.__doc__ is not None)
assert(RegularPolygon.get_interior_angle.__doc__ is not None)
assert(RegularPolygon.get_total_interior_angle.__doc__ is not None)
