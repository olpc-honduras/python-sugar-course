# This variable's customisable to change the language of the outputted files.
# Permissible values: 'en' or 'es'
LANG = es

PDFS = presentation.pdf handout.pdf
EN_EXERCISES = \
	1_printing-names \
	2_arithmetic \
	3_squares \
	4_fibonacci \
	5_while \
	6_break \
	7_lists \
	8_lists-min \
	9_dicts \
	10_def \
	11_recursion \
	12_create-class \
	13_extend-class \
	14_property \
	15_private-methods \
	$(NULL)
ES_EXERCISES = \
	1_imprimir-nombres \
	2_aritmetica \
	3_cuadrados \
	4_fibonacci \
	5_while \
	6_break \
	7_listas \
	8_listas-min \
	9_dicts \
	$(NULL)

TEX_FILES = \
	slides.tex \
	introduction-to-python.tex \
	python-functions-and-classes.tex \
	writing-sugar-activities.tex \
	$(NULL)

IMAGE_FILES = \
	simple-gtk-program.png \
	$(NULL)

ZIPS = \
	python-ejercicios.zip \
	python-exercises.zip \
	$(NULL)
EN_PYTHON_FILES = \
	$(addprefix exercises/en/,$(addsuffix .py,$(EN_EXERCISES))) \
	$(addprefix exercises/en/,$(addsuffix _solution.py,$(EN_EXERCISES))) \
	$(NULL)

ES_PYTHON_FILES = \
	$(addprefix exercises/es/,$(addsuffix .py,$(ES_EXERCISES))) \
	$(addprefix exercises/es/,$(addsuffix _solucion.py,$(ES_EXERCISES))) \
	$(NULL)

all: $(PDFS) $(ZIPS)

presentation.pdf: presentation.tex $(TEX_FILES) $(IMAGE_FILES)
	pdflatex "\providecommand\locale{$(LANG)}\input{$<}"
	pdflatex "\providecommand\locale{$(LANG)}\input{$<}"

handout.pdf: handout.tex $(TEX_FILES) $(IMAGE_FILES)
	pdflatex "\providecommand\locale{$(LANG)}\input{$<}"
	pdflatex "\providecommand\locale{$(LANG)}\input{$<}"

# Zip up the exercises.
python-exercises.zip: $(EN_PYTHON_FILES)
	zip --quiet --no-dir-entries $@ $^

python-ejercicios.zip: $(ES_PYTHON_FILES)
	zip --quiet --no-dir-entries $@ $^

check:
	$(foreach ex,$(EN_EXERCISES),pep8 exercises/en/${ex}.py exercises/en/${ex}_solution.py;)
	$(foreach ex,$(ES_EXERCISES),pep8 exercises/es/${ex}.py exercises/es/${ex}_solucion.py;)

clean:
	rm -f $(PDFS:.pdf=.aux)
	rm -f $(PDFS:.pdf=.log)
	rm -f $(PDFS:.pdf=.out)
	rm -f $(PDFS:.pdf=.nav)
	rm -f $(PDFS:.pdf=.snm)
	rm -f $(PDFS:.pdf=.toc)
	rm -f $(PDFS:.pdf=.vrb)
	rm -f $(PDFS)
	rm -f $(ZIPS)

.PHONY: all clean check
